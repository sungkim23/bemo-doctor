package com.tech.sungkim.bemoprovider.util;


import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tech.sungkim.bemoprovider.R;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StaticMethods {


    public static  String convertTime(long time){
        Date date = new Date(time);
        Format format = new SimpleDateFormat("yyyy MM dd"); //new SimpleDateFormat("yyyy MM dd HH:mm:ss");
        return format.format(date);
    }

    public static void designRegisterLogin(TextView txt1, TextView txt2, View view1, View view2,
                                           CardView login , CardView register, int type, Context context){
        txt1.setTextColor(type==1 ? ContextCompat.getColor(context, R.color.bemo_light)
                :  ContextCompat.getColor(context, R.color.gray_dark800));
        txt2.setTextColor(type==2 ? ContextCompat.getColor(context, R.color.bemo_light)
                :  ContextCompat.getColor(context, R.color.gray_dark800));
        view1.setVisibility(type==1 ? View.VISIBLE : View.INVISIBLE);
        view2.setVisibility(type==2 ? View.VISIBLE : View.INVISIBLE);

        login.setVisibility(type ==1 ? View.VISIBLE : View.GONE);
        register.setVisibility(type == 2 ? View.VISIBLE : View.GONE);
    }


    public static void msgSnackbar(View v, String msg){
        Snackbar snackbar = Snackbar.make(v, msg, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(Color.RED);
        snackbar.show();
    }

    public static boolean isValidEmail(View view, String email) {
        boolean isGoodEmail = (email != null && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches());
        if (!isGoodEmail) {
            StaticMethods.msgSnackbar(view,"Please enter a valid email address");
            return false;
        }
        return true;
    }

    public static boolean isValidPassword(View view, String password) {
        if (TextUtils.isEmpty(password)){
            StaticMethods.msgSnackbar(view,"Enter password!");
            return false ;
        }
        else if (password.length() < 6) {
            StaticMethods.msgSnackbar(view,"Password should be longer than 6 characters");
            return false;
        }
        return true;
    }

    public static void showErrorMessageToUser(String errorMessage, Context context){

        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        builder.setMessage(errorMessage)
                //.setTitle("")
                .setPositiveButton(android.R.string.ok, null);
        AlertDialog dialog=builder.create();
        dialog.show();
    }

    public static boolean isValidDate(String date, View view){
        if(date!=null && date.length()>3) return true;
        else {
            StaticMethods.msgSnackbar(view,"Enter Birth date!");
            return false;
        }
    }

    public static boolean isValidFullName(String name, View view){
        if(name!=null && name.length()>3) return true;
        else {
            StaticMethods.msgSnackbar(view,"Name can't be less than 3 characters!");
            return false;
        }
    }

    public static boolean isValidYear(String year, View view){
        if(year!=null && year.length()>3) return true;
        else {
            StaticMethods.msgSnackbar(view,"Select year started counnseling");
            return false;
        }
    }

    public static boolean isValidSpinner(String spinner, View view){
        if(spinner!=null && spinner.length()>2) return true;
        else {
            StaticMethods.msgSnackbar(view,"Select a profession");
            return false;
        }
    }


    public static void selectChangeColor(CardView cardDay, TextView txt_day, ImageView imgDay, Context context,
                                         CardView card_night, TextView txtNight, ImageView imgNight, int tipo){


        cardDay.setCardBackgroundColor(tipo == 1 ? ContextCompat.getColor(context, R.color.color_provider_blue) : ContextCompat.getColor(context, R.color.gray_light_1));
        txt_day.setTextColor(tipo == 1 ? ContextCompat.getColor(context, R.color.white) : ContextCompat.getColor(context, R.color.gray));
        imgDay.setImageDrawable(tipo == 1 ? ContextCompat.getDrawable(context, R.mipmap.ico_day) : ContextCompat.getDrawable(context, R.mipmap.ico_day_gray));

        card_night.setCardBackgroundColor(tipo == 0 ? ContextCompat.getColor(context, R.color.color_provider_blue) : ContextCompat.getColor(context, R.color.gray_light_1));
        txtNight.setTextColor(tipo == 0 ? ContextCompat.getColor(context, R.color.white) : ContextCompat.getColor(context, R.color.gray));
        imgNight.setImageDrawable(tipo == 0 ? ContextCompat.getDrawable(context, R.mipmap.ico_night) : ContextCompat.getDrawable(context, R.mipmap.ico_night_gray));


    }

    public static boolean isInternetAvailable(Context context) {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }



}
