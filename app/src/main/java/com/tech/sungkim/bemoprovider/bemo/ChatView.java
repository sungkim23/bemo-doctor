package com.tech.sungkim.bemoprovider.bemo;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tech.sungkim.bemoprovider.R;
import com.tech.sungkim.bemoprovider.adapters.AdapterMessage;
import com.tech.sungkim.bemoprovider.bus.BusFinishSession;
import com.tech.sungkim.bemoprovider.bus.BusProvider;
import com.tech.sungkim.bemoprovider.model.MessageChat;
import com.tech.sungkim.bemoprovider.model.Provider;
import com.tech.sungkim.bemoprovider.model.User;
import com.tech.sungkim.bemoprovider.util.DialogStatic;
import com.tech.sungkim.bemoprovider.util.PathFirebase;
import com.tech.sungkim.bemoprovider.util.ProviderManager;
import com.tech.sungkim.bemoprovider.util.StaticMethods;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ChatView extends AppCompatActivity implements View.OnClickListener {

    public Toolbar toolbar;
    private RecyclerView recyclerView;
    public ImageButton imgbtn_sendPhoto;
    public ImageButton imgbtn_sendchat;
    private EditText edit_message;
    private String idRecord = "";
    private User user;
    private int SENDER = 0;
    private int RECEIVE = 1;
    private AdapterMessage adapter;
    public DatabaseReference pathFirebase;
    private DatabaseReference pathMessages;
    private DatabaseReference pathRecords;
    private DatabaseReference pathUserInfo;

    private ChildEventListener messageListener;
    private RelativeLayout relativelayout;
    private DialogStatic dialogStatic;

    public static final String FCM_MESSAGE_URL = "https://fcm.googleapis.com/fcm/send";
    final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private Boolean sendNotification= true;
    private String idChat;
    Provider providerLocal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatview);
        BusProvider.getInstance().register(this);
        ini();

        List<MessageChat> emptyMessageChat=new ArrayList<>();
        adapter = new AdapterMessage(emptyMessageChat);
        recyclerView.setAdapter(adapter);

        pathFirebase =  FirebaseDatabase.getInstance().getReferenceFromUrl(PathFirebase.PATH_FIREBASE);
        pathUserInfo = pathFirebase.child(PathFirebase.PATH_USER);

        pathMessages = pathFirebase.child(PathFirebase.PATH_CHAT)
                .child(createPathMessage(user.getIdUser(), providerLocal.getId())).child(idChat);

        pathRecords = pathFirebase.child(PathFirebase.PATH_RECORD).child(PathFirebase.PATH_RECORD_USER);

        dialogStatic.show();
        messageListener = pathMessages.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.exists()){

                    MessageChat newMessage = dataSnapshot.getValue(MessageChat.class);
                    if(newMessage.getSender().equals(user.getIdUser())) newMessage.setSenderOrRecipient(SENDER);
                    else newMessage.setSenderOrRecipient(RECEIVE);
                    adapter.refillAdapter(newMessage);
                    recyclerView.scrollToPosition(adapter.getItemCount()-1);
                }
                dialogStatic.dissmis();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {dialogStatic.dissmis();}
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {dialogStatic.dissmis();}
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {dialogStatic.dissmis();}
            @Override
            public void onCancelled(DatabaseError databaseError) {dialogStatic.dissmis();}
        });

        dialogStatic.show();
        updateDataChatInProvider("UNAVAILABLE", "PROCESSING");//Cuando inicia la session=> ponerse unavailable
        createOrUpdateRecord("OPEN");



        pathUserInfo.child(user.getIdUser()).child("chatAttention").child("state").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    System.out.println("Salida Use: " + dataSnapshot.getValue());
                    if (dataSnapshot.getValue().equals("CLOSE")){
                        StaticMethods.showErrorMessageToUser("User has just finished the session", ChatView.this);
                        Toast.makeText(getApplicationContext(), "USER CLOSE SESSION", Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void ini(){
        dialogStatic = new DialogStatic(ChatView.this, "", "Loading");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_gray);
        //getSupportActionBar().setTitle("View Chat session");
        toolbar.setTitleTextColor(ContextCompat.getColor(getApplicationContext(), R.color.gray_dark));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StaticMethods.msgSnackbar(relativelayout,"Currently in attention");
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        edit_message = (EditText) findViewById(R.id.edit_message);
        imgbtn_sendchat = (ImageButton) findViewById(R.id.imgbtn_sendchat);
        imgbtn_sendPhoto = (ImageButton) findViewById(R.id.imgbtn_sendPhoto);
        relativelayout = (RelativeLayout) findViewById(R.id.relativelayout);
        imgbtn_sendPhoto.setOnClickListener(this);
        imgbtn_sendchat.setOnClickListener(this);


        providerLocal =  new ProviderManager(ChatView.this).getProviderData();

        Bundle extras = getIntent().getExtras();
        if (extras!=null){
            user = (User) extras.get("user");
            String session = extras.getString("session");

            if (session!=null && session.equals("PAST_SESSIONS")){
                idChat = user.getChatAttention().getIdChat();
                idRecord = user.getChatAttention().getIdRecord();
            }else {//from notification
                idChat = extras.getString("idChat");
                idRecord= extras.getString("idRecord");
            }
            getSupportActionBar().setTitle(user.getFullName());
        }else {
            StaticMethods.msgSnackbar(relativelayout, getResources().getString(R.string.warning_chat_view));
        }
        System.out.println("Salida user: " + user.getIdUser() + " / " + user.getEmail() + " / " + idChat + " / ? "  + idRecord);
    }


    private String createPathMessage(String idUser, String idProvider){
        return idUser+"-"+idProvider;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imgbtn_sendchat:
                sendMessage();
                break;

            case R.id.imgbtn_sendPhoto:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BusProvider.getInstance().unregister(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_close_chat:

                new AlertDialog.Builder(this).setIcon(R.mipmap.logo_bemo)
                        .setMessage("Estas seguro de finalizar la atencion?")
                        .setPositiveButton("Finalizar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (messageListener!=null && pathMessages!=null){
                                    pathMessages.removeEventListener(messageListener);
                                }
                                createOrUpdateRecord("CLOSE");
                                updateDataChatInProvider("AVAILABLE", "CLOSE");//se pondra disponible

                                BusProvider.getInstance().post( new BusFinishSession(true));

                            }
                        })
                        .setNegativeButton("Permanecer", null)
                        .show();
                break;

            case android.R.id.home:
                ChatView.this.finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void sendMessage(){
        String sendMessage = edit_message.getText().toString().trim();

        if(!sendMessage.isEmpty() && user.getIdUser() != null ){

            if (sendNotification) {
                /**sendMessageFCM(sendMessage);*/
                sendNotification = false;
            }

            Map<String, Object> newMessage = new HashMap<>();
            newMessage.put("message", sendMessage);
            newMessage.put("receive", user.getIdUser());
            newMessage.put("sender", providerLocal.getId());
            newMessage.put("visto", false);//False = visto,  TRUE = no visto msg.
            newMessage.put("name", providerLocal.getFullName());
            newMessage.put("timewrite", String.valueOf(new Date().getTime()));

            pathMessages.push().setValue(newMessage);
            edit_message.setText("");

            InputMethodManager mgr = (InputMethodManager)getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            mgr.hideSoftInputFromWindow(edit_message.getWindowToken(), 0);
        }else {
            StaticMethods.msgSnackbar(relativelayout, "Enter the message");
        }
    }


    private void updateDataChatInProvider(final String status, final String state){

        HashMap<String, Object> providerObj = new HashMap<>();
        providerObj.put("status", status);//cuando va responder la session => no podra atender a otro usuario.
        providerObj.put("chatAttention/state", state);

        pathFirebase.child(PathFirebase.PATH_PROVIDER).child(providerLocal.getId())
                .updateChildren(providerObj, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                dialogStatic.dissmis();
                if (state.equals("CLOSE")) ChatView.this.finish();
            }
        });

    }


    private void createOrUpdateRecord(String state){//1er paso(registramos) y 2do paso actualizamos(al cancelar o finalizar)
        //Registramos en el historial de atenciones:
        pathRecords.child(user.getIdUser()).child(idRecord).child("state").setValue(state);

        /** Change status by state*/
        pathRecords.child(providerLocal.getId()).child(idRecord).child("state").setValue(state);

        if (state.equals("CLOSEZX")){
            pathFirebase.child(PathFirebase.PATH_USER).child(user.getIdUser())
                    .child("chatAttention/state").setValue(state);
        }
        pathFirebase.child(PathFirebase.PATH_USER).child(user.getIdUser())
                .child("chatAttention/state").setValue(state);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode==KeyEvent.KEYCODE_BACK) {
            if ("peru".equals("chile")){
                //MapsCMedicos.this.finish();
                return true;
            }else {
                Toast.makeText(ChatView.this, "Currently session online", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public void sendMessageFCM(final String message) {

        new AsyncTask<String, String, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialogStatic.show();
            }

            @Override
            protected String doInBackground(String... params) {
                try {
                    JSONObject mainJson = new JSONObject();
                    mainJson.put("to", user.getGcmId());

                    JSONObject notification = new JSONObject();
                    notification.put("body", providerLocal.getFullName());
                    notification.put("title", "BEMO APP");

                    JSONObject data = new JSONObject();
                    data.put("message", message);
                    data.put("username", providerLocal.getFullName());
                    data.put("email", providerLocal.getEmail());
                    data.put("path", createPathMessage(user.getIdUser(), providerLocal.getId()));
                    data.put("idprovider", providerLocal.getId());
                    data.put("idChat", idChat);

                    //mainJson.put("notification", notification);
                    mainJson.put("data", data);
                    String result = postToFCM(mainJson.toString());
                    System.out.println("Salida notifiy GCM: " + mainJson);
                    return result;
                } catch (Exception ex) {ex.printStackTrace(); }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                dialogStatic.dissmis();
                try {
                    JSONObject resultJson = new JSONObject(result);
                    int success, failure;
                    success = resultJson.getInt("success");
                    failure = resultJson.getInt("failure");
                    if (success==1) sendNotification = false;
                    if (success==0) sendNotification = true;
                    Toast.makeText(ChatView.this, "Message Success: " +success + "Message Failed: " + failure, Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ChatView.this, "Message Failed," + " Unknown error occurred.", Toast.LENGTH_LONG).show();
                    sendNotification = true;
                }
            }
        }.execute();
    }

    public String postToFCM(String bodyString) throws IOException {

        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(JSON, bodyString);
        Request request = new Request.Builder()
                .url(FCM_MESSAGE_URL)
                .post(body)
                .addHeader("Authorization", "key=" + "AIzaSyDtfHda6r0fL8jHCstO2OOKqWhEL4S4Lv0")
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

}
