package com.tech.sungkim.bemoprovider.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.tech.sungkim.bemoprovider.R;
import com.tech.sungkim.bemoprovider.bemo.ProviderDrawer;


public class MyFirebaseMessaging extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

         Log.d(TAG, "From: " + remoteMessage.getFrom());
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        if (remoteMessage.getData().get("message")!=null){
            sendNotification(
                    remoteMessage.getData().get("message"),
                    remoteMessage.getData().get("username"),
                    "BEMO APP USER ",
                    remoteMessage.getData().get("iduser"),
                    remoteMessage.getData().get("email"),
                    remoteMessage.getData().get("keyPathMessage"),
                    remoteMessage.getData().get("idRecord"),
                    remoteMessage.getData().get("idGCM"));
        }

    }


    private void sendNotification(String messageBody, String username, String title, String iduser,
                                  String email, String keyPathMessage, String idRecord, String  idGCM) {
        Intent intent = new Intent(this, ProviderDrawer.class);
        intent.setAction("chat_user");
        intent.putExtra("iduser", iduser);
        intent.putExtra("email", email);
        intent.putExtra("username", username);
        intent.putExtra("keyPathMessage", keyPathMessage);
        intent.putExtra("idRecord", idRecord);
        intent.putExtra("fcmIdUser",idGCM);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.user_noti)
                .setContentTitle(title)
                .setContentText(username + ": " + messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}