package com.tech.sungkim.bemoprovider.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.tech.sungkim.bemoprovider.R;

import java.util.Calendar;
import java.util.Locale;


public class Dashboard extends Fragment {


    TextView txt_value_session;
    TextView txt_value_rating;
    SwitchCompat switch_online;
    TextView txt_number;
    TextView txt_date;
    boolean varSwitch = true;

    public Dashboard() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fr_dashboard, container, false);
        txt_value_session = (TextView) view.findViewById(R.id.txt_value_session);
        txt_value_rating = (TextView) view.findViewById(R.id.txt_value_rating);
        switch_online = (SwitchCompat) view.findViewById(R.id.switch_online);
        txt_number = (TextView) view.findViewById(R.id.txt_number);
        txt_date = (TextView) view.findViewById(R.id.txt_date);



        switch_online.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) varSwitch = true;
                else varSwitch = false;
            }
        });

        getCurentlyDate();
        return view;
    }



    private void getCurentlyDate(){
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);
        int date = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);
        txt_date.setText(date + "/" + month + "/" + year);
    }
}
