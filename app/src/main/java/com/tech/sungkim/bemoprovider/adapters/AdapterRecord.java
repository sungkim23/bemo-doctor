package com.tech.sungkim.bemoprovider.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tech.sungkim.bemoprovider.R;
import com.tech.sungkim.bemoprovider.bemo.ChatView;
import com.tech.sungkim.bemoprovider.model.RecordProvider;
import com.tech.sungkim.bemoprovider.util.StaticMethods;

import java.util.List;

/** Created by jacobi on 23/10/2016. */

public class AdapterRecord extends RecyclerView.Adapter<AdapterRecord.ViewHolderUsers> {

    private List<RecordProvider> listRecord;
    private Context context;


    public AdapterRecord(Context context, List<RecordProvider> listRecord) {
        this.listRecord = listRecord;
        this.context = context;
    }

    @Override
    public AdapterRecord.ViewHolderUsers onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AdapterRecord.ViewHolderUsers(context, LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_record, parent, false));
    }

    @Override
    public void onBindViewHolder(AdapterRecord.ViewHolderUsers holder, int position) {

        RecordProvider fireProvider = listRecord.get(position);

        holder.getTxt_fullname().setText(fireProvider.getNameUser());
        holder.getTxt_calification().setText(fireProvider.getRatingUser());
        holder.getTxt_lastsession().setText(StaticMethods.convertTime(fireProvider.getDateCreate()));
        holder.getTxt_lastsession().setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return listRecord.size();
    }

    public void setfill(RecordProvider provider) {
        //listRecord.add(0,provider);
        listRecord.add(provider);
        notifyDataSetChanged();
    }

    public void addFirstPosition(RecordProvider provider) {
        listRecord.add(0,provider);
        //listRecord.add(provider);
        notifyDataSetChanged();
    }

    public void updateData(int index, RecordProvider record) {
        listRecord.set(index, record);
        notifyDataSetChanged();
    }


    public class ViewHolderUsers extends RecyclerView.ViewHolder implements View.OnClickListener{


        private Context contextHolder;
        private ImageView img_providerPhoto;

        private TextView txt_lastsession;
        private TextView txt_calification;
        private TextView txt_fullname;

        private ViewHolderUsers(Context context, View itemView) {
            super(itemView);
            img_providerPhoto = (ImageView)itemView.findViewById(R.id.img_providerPhoto);
            txt_lastsession = (TextView) itemView.findViewById(R.id.txt_lastsession);
            txt_calification = (TextView) itemView.findViewById(R.id.txt_calification);
            txt_fullname = (TextView)itemView.findViewById(R.id.txt_fullname);

            txt_calification.setVisibility(View.GONE);
            contextHolder=context;
            itemView.setOnClickListener(this);
        }

        public Context getContextHolder() {
            return contextHolder;
        }

        public void setContextHolder(Context contextHolder) {
            this.contextHolder = contextHolder;
        }

        public ImageView getImg_providerPhoto() {
            return img_providerPhoto;
        }

        public void setImg_providerPhoto(ImageView img_providerPhoto) {
            this.img_providerPhoto = img_providerPhoto;
        }

        public TextView getTxt_lastsession() {
            return txt_lastsession;
        }

        public void setTxt_lastsession(TextView txt_lastsession) {
            this.txt_lastsession = txt_lastsession;
        }

        public TextView getTxt_calification() {
            return txt_calification;
        }

        public void setTxt_calification(TextView txt_calification) {
            this.txt_calification = txt_calification;
        }

        public TextView getTxt_fullname() {
            return txt_fullname;
        }

        public void setTxt_fullname(TextView txt_fullname) {
            this.txt_fullname = txt_fullname;
        }

        @Override
        public void onClick(View view) {

            int position = getLayoutPosition();
            RecordProvider provider = listRecord.get(position);
            Intent chatIntent = new Intent(contextHolder, ChatView.class);
            //chatIntent.putExtra("provider_data", provider);
            //chatIntent.putExtra(ReferenceURL.KEY_PASS_USERS_INFO, user);
            //contextHolder.startActivity(chatIntent);
        }
    }

}