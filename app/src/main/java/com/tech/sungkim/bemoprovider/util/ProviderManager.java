package com.tech.sungkim.bemoprovider.util;


import android.content.Context;
import android.content.SharedPreferences;

import com.tech.sungkim.bemoprovider.model.Provider;

public class ProviderManager {


    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private static int PRIVATE_MODE = 1;
    private static final String PREFERENCE_NAME = "Provider_Data";

    private static final String NAME_PROVIDER = "nameProvider";
    private static final String EMAIL_PROVIDER = "emailProvider";
    private static final String GENDER_PROVIDER = "genderProvider";
    public static final String KEY_PROVIDER = "keyProvider"; // Path
    private static final String SPECIALTY_PROVIDER = "specialtyProvider"; // Path

    public ProviderManager(Context context) {//Intialize
        sharedPreferences = context.getSharedPreferences(PREFERENCE_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public void saveDataProvider(String name, String email, String specialty, String gender, String key,
                                 int schedule, String pick_days){
        editor.putString(NAME_PROVIDER, name);
        editor.putString(EMAIL_PROVIDER, email);
        editor.putString(SPECIALTY_PROVIDER, specialty);
        editor.putString(GENDER_PROVIDER, gender);
        editor.putString(KEY_PROVIDER, key);
        editor.putInt(PathFirebase.KEY_SCHEDULE, schedule);
        editor.putString(PathFirebase.kEY_PICK_DAYS, pick_days);
        editor.apply();
    }

    public void saveSomeData(String key, String value){
        editor.putString(key, value);
        editor.apply();
    }

    public void saveIntData(String key, int value){
        editor.putInt(key, value);
        editor.apply();
    }

    public String getSomeData(String key){
        return sharedPreferences.getString(key, null);
    }

    public int getIntData(String key){
        return sharedPreferences.getInt(key, 0);
    }

    public Provider getProviderData(){

        Provider provider = new Provider();
        provider.setId(sharedPreferences.getString(KEY_PROVIDER, null));
        provider.setFullName(sharedPreferences.getString(NAME_PROVIDER, null));
        provider.setEmail(sharedPreferences.getString(EMAIL_PROVIDER, null));
        provider.setGender(sharedPreferences.getString(GENDER_PROVIDER, null));
        provider.setSpecialty(sharedPreferences.getString(SPECIALTY_PROVIDER, null));
        return provider;
    }


    public void clearProviderData(){
        sharedPreferences.edit().clear().apply();
    }

}
