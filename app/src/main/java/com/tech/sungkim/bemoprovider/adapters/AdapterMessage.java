package com.tech.sungkim.bemoprovider.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tech.sungkim.bemoprovider.R;
import com.tech.sungkim.bemoprovider.model.MessageChat;

import java.util.List;

/**
   Created by jacob on 18/09/2016.
 */

public class AdapterMessage extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<MessageChat> listMessage;
    private static final int SENDER = 0;
    private static final int RECIPIENT = 1;

    public AdapterMessage(List<MessageChat> listOfFireChats) {
        listMessage=listOfFireChats;
    }

    @Override
    public int getItemViewType(int position) {
        if(listMessage.get(position).getSenderOrRecipient()==SENDER){
            return SENDER;
        }else {
            return RECIPIENT;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (viewType) {
            case SENDER:
                View viewSender = inflater.inflate(R.layout.row_sendchat, viewGroup, false);
                viewHolder= new ViewHolderSender(viewSender);
                break;
            case RECIPIENT:
                View viewRecipient = inflater.inflate(R.layout.row_receivechat, viewGroup, false);
                viewHolder=new ViewHolderReceive(viewRecipient);
                break;
            default:
                View viewSenderDefault = inflater.inflate(R.layout.row_sendchat, viewGroup, false);
                viewHolder= new ViewHolderSender(viewSenderDefault);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        switch (viewHolder.getItemViewType()){
            case SENDER:
                ViewHolderSender viewHolderSender=(ViewHolderSender)viewHolder;
                senderView(viewHolderSender,position);
                break;
            case RECIPIENT:
                ViewHolderReceive viewHolderReceive = (ViewHolderReceive)viewHolder;
                recipientView(viewHolderReceive,position);
                break;
        }
    }

    private void senderView(ViewHolderSender viewHolder, int position) {
        MessageChat senderMessage = listMessage.get(position);
        viewHolder.getTxt_sendchat().setText(senderMessage.getMessage());
    }

    private void recipientView(ViewHolderReceive viewHolder, int position) {
        MessageChat receiveMessage = listMessage.get(position);
        viewHolder.getTxt_receive().setText(receiveMessage.getMessage());
    }

    @Override
    public int getItemCount() {
        return listMessage.size();
    }


    public void refillAdapter(MessageChat newFireChatMessage){

        listMessage.add(newFireChatMessage);
        notifyItemInserted(getItemCount()-1);
    }


    public class ViewHolderSender extends RecyclerView.ViewHolder {

        public TextView txt_sendchat;

        public ViewHolderSender(View itemView) {
            super(itemView);
            txt_sendchat=(TextView)itemView.findViewById(R.id.txt_sendchat);
        }

        public TextView getTxt_sendchat() {
            return txt_sendchat;
        }

    }


    public class ViewHolderReceive extends RecyclerView.ViewHolder {

        public TextView txt_receive;

        public ViewHolderReceive(View itemView) {
            super(itemView);
            txt_receive=(TextView)itemView.findViewById(R.id.txt_receive);
        }

        public TextView getTxt_receive() {
            return txt_receive;
        }

    }



}