package com.tech.sungkim.bemoprovider.model;

import java.io.Serializable;

/**
  Created by jacob on 18/09/2016.
 */

public class User implements Serializable{

    private String idUser;
    private String fullName;
    private String age;
    private String email;
    //private String provider;/** Obs Es un array y no String*/
    private Long dateRegister;/***///fecha de registro
    private String gender;
    private String gcmId;
    private String status;
    private String connection;

    private ChatAttention chatAttention;

    public String getGcmId() {
        return gcmId;
    }

    public void setGcmId(String gcmId) {
        this.gcmId = gcmId;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }



    public Long getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(Long dateRegister) {
        this.dateRegister = dateRegister;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public ChatAttention getChatAttention() {
        return chatAttention;
    }

    public void setChatAttention(ChatAttention chatAttention) {
        this.chatAttention = chatAttention;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getConnection() {
        return connection;
    }

    public void setConnection(String connection) {
        this.connection = connection;
    }
}
