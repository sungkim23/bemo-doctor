package com.tech.sungkim.bemoprovider.adapters;


import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.tech.sungkim.bemoprovider.model.Specialty;

import java.util.List;

public class SpinnerAdapter extends ArrayAdapter<Specialty> {

    private Context context;
    // Your custom values for the spinner (User)
    private List<Specialty> values;

    public SpinnerAdapter(Context context, int textViewResourceId, List<Specialty> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    public int getCount(){
        return values.size();
    }

    public Specialty getItem(int position){
        return values.get(position);
    }

    public long getItemId(int position){
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = new TextView(context);
        label.setTextColor(Color.BLACK);
        label.setText(values.get(position).getCareer());

        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = new TextView(context);
        label.setTextColor(Color.BLACK);
        label.setText(values.get(position).getCareer());
        return label;
    }
}