package com.tech.sungkim.bemoprovider.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tech.sungkim.bemoprovider.R;
import com.tech.sungkim.bemoprovider.adapters.AdapterRecord;
import com.tech.sungkim.bemoprovider.model.RecordProvider;
import com.tech.sungkim.bemoprovider.util.DialogStatic;
import com.tech.sungkim.bemoprovider.util.PathFirebase;

import java.util.ArrayList;
import java.util.List;


public class Conversation extends Fragment {


    private DatabaseReference mainPathFB;
    private String idProvider;
    private FirebaseAuth auth;

    public TextView txt_infoprovicer;
    public RecyclerView recyclerView;
    public AdapterRecord adapterRecord;
    public List<RecordProvider> listRecord;
    public List<String> listUpdate;
    public DatabaseReference recordPath;
    public ChildEventListener recordListener;

    DialogStatic dialogStatic;

    public Conversation() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fr_conversation, container, false);
        dialogStatic = new DialogStatic(getActivity(), "", "Loading");
        txt_infoprovicer = (TextView) view.findViewById(R.id.txt_infoprovicer);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);


        auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser()!=null) idProvider = auth.getCurrentUser().getUid();

        mainPathFB = FirebaseDatabase.getInstance().getReferenceFromUrl(PathFirebase.PATH_FIREBASE);
        recordPath = mainPathFB.child(PathFirebase.PATH_RECORD).child(PathFirebase.PATH_RECORD_PROVIDER);


        //dialogStatic.show();
        //Necessary for LogOut to work

        listUpdate = new ArrayList<>();
        listRecord = new ArrayList<>();
        adapterRecord = new AdapterRecord(getActivity(), listRecord);
        recyclerView.setAdapter(adapterRecord);

        //Por el momento solo mostraremos las atenciones que hizo el provider sin ver los detalles del chat
        //Query query = recordPath.orderByChild("idProvider").equalTo(idProvider);
        //query.addChildEventListener(new ChildEventListener()...   or
        System.out.println("Salida idProvider: " + idProvider );

        recordListener = recordPath.child(idProvider).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.exists()){
                    txt_infoprovicer.setVisibility(View.GONE);
                    RecordProvider record = dataSnapshot.getValue(RecordProvider.class);
                    record.setId(dataSnapshot.getKey());

                    System.out.println("Salida user: " + record.getNameUser() + " / " + record.getState());
                    if (record.getState()!=null && record.getState().equals("CLOSE")){
                        adapterRecord.setfill(record);
                        //listRecord.add(record);
                        //listUpdate.add(record);
                    }else {
                        listUpdate.add(record.getId());//Solo los q estan abiertos(ya q son los unicos q pueden cambiar de estado)
                    }

                }else {
                    dialogStatic.dissmis();
                    System.out.println("Salida historial nulo ---------- ");
                }
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                //Mejorar las actualizaciones para q no se repitan en el record
                /**if (dataSnapshot.exists()){
                 RecordProvider obj =  dataSnapshot.getValue(RecordProvider.class);
                 String id = dataSnapshot.getKey();
                 System.out.println("Salida change value record: " + obj.getNameUser());
                 obj.setId(id);
                 if (obj.getStatus().equals("CLOSE")){
                 adapterRecord.addFirstPosition(obj);
                 }
                 }*/
            }
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {}
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
            @Override
            public void onCancelled(DatabaseError databaseError) { dialogStatic.dissmis();}
        });




        return view;
    }

}
