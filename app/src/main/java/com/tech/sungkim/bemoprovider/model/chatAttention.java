package com.tech.sungkim.bemoprovider.model;

import java.io.Serializable;

/** Created by jacobi on 6/11/2016.
 */

public class ChatAttention implements Serializable{

    private String idRecord;
    private String idProvider;
    private String idChat;
    private String idUser;
    private String state;

    private Long startAttention; //Tiempo de inicio del chat
    private Long consultTime;

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdChat() {
        return idChat;
    }

    public void setIdChat(String idChat) {
        this.idChat = idChat;
    }

    public String getIdProvider() {
        return idProvider;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setIdProvider(String idProvider) {
        this.idProvider = idProvider;
    }

    public Long getStartAttention() {
        return startAttention;
    }

    public void setStartAttention(Long startAttention) {
        this.startAttention = startAttention;
    }

    public Long getConsultTime() {
        return consultTime;
    }

    public void setConsultTime(Long consultTime) {
        this.consultTime = consultTime;
    }

    public String getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(String idRecord) {
        this.idRecord = idRecord;
    }
}
