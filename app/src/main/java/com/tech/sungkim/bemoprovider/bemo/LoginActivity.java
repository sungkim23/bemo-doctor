package com.tech.sungkim.bemoprovider.bemo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.tech.sungkim.bemoprovider.R;
import com.tech.sungkim.bemoprovider.adapters.SpinnerAdapter;
import com.tech.sungkim.bemoprovider.model.Provider;
import com.tech.sungkim.bemoprovider.model.Specialty;
import com.tech.sungkim.bemoprovider.util.DialogStatic;
import com.tech.sungkim.bemoprovider.util.PathFirebase;
import com.tech.sungkim.bemoprovider.util.ProviderManager;
import com.tech.sungkim.bemoprovider.util.StaticMethods;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout layout_login, layout_register;
    private TextView txt_login, txt_register;
    public TextView txt_haveaccount, txt_login_;
    private View view_1, view_2;
    private CardView cardview_login, cardview_register;
    private EditText email_login, password_login, email_register,
            password_register, edit_fullname, year_counseling;
    private Button button_login, button_register;
    private FirebaseAuth auth;
    DialogStatic dialogStatic;
    public RadioGroup radioGroup;
    private String gender = "Male";
    private DatabaseReference pathFirebase;
    private Spinner professionSpinner;

    public List<Specialty> listSpecialty;
    public Specialty specialtySelect;
    SpinnerAdapter adapterSpinn;
    private FirebaseAuth.AuthStateListener authListener;


    private DatabaseReference mainPathFB;
    public ChildEventListener recordListener;
    public DatabaseReference recordPath;

    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initializationView();



        pathFirebase = FirebaseDatabase.getInstance().getReferenceFromUrl(PathFirebase.PATH_FIREBASE);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        //loadDataServiceType();


        auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null) {

            String uid = auth.getCurrentUser().getUid();
            Map<String, Object> updates = new HashMap<>();
            updates.put("gcmId",refreshedToken );
            if (refreshedToken!=null && !refreshedToken.isEmpty()){
                DatabaseReference ref = pathFirebase.child(PathFirebase.PATH_PROVIDER).child(uid);
                ref.updateChildren(updates);
            }else System.out.println("Salida oncreate Vacio::" + refreshedToken);

            startActivity(new Intent(LoginActivity.this, ProviderDrawer.class));
            finish();
        }


        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    new ProviderManager(LoginActivity.this).clearProviderData();
                    StaticMethods.showErrorMessageToUser("You don't have access to a account of provider",
                            LoginActivity.this);
                }
            }
        };





        /*adapterSpinn = new SpinnerAdapter(LoginActivity.this, android.R.layout.simple_spinner_item,
                listSpecialty);
        professionSpinner.setAdapter(adapterSpinn);
        professionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                Specialty user = adapterSpinn.getItem(position);
                specialtySelect = adapterSpinn.getItem(position);
                Toast.makeText(getApplicationContext(), "ID: " + user.getId() + "\nName: "
                        + user.getCareer(), Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });*/




    }

    private void initializationView(){
        listSpecialty = new ArrayList<>();

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        txt_login = (TextView) findViewById(R.id.txt_login);
        txt_register = (TextView) findViewById(R.id.txt_register);
        view_1 = findViewById(R.id.view_1);
        view_2 = findViewById(R.id.view_2);
        cardview_login = (CardView) findViewById(R.id.cardview_login);
        cardview_register = (CardView) findViewById(R.id.cardview_register);
        email_login = (EditText) findViewById(R.id.email_login);
        password_login = (EditText) findViewById(R.id.password_login);
        email_register = (EditText) findViewById(R.id.email_register);
        password_register = (EditText) findViewById(R.id.password_register);
        edit_fullname = (EditText) findViewById(R.id.edit_fullname);
        year_counseling = (EditText) findViewById(R.id.year_counseling);
        txt_haveaccount = (TextView) findViewById(R.id.txt_haveaccount);
        txt_login_ = (TextView) findViewById(R.id.txt_login_);
        button_login = (Button) findViewById(R.id.button_login);
        button_register = (Button) findViewById(R.id.button_register);
        professionSpinner = (Spinner)findViewById(R.id.professionSpinner);
        dialogStatic = new DialogStatic(LoginActivity.this, "", "Loading...");

        layout_login = (LinearLayout) findViewById(R.id.layout_login);
        layout_register = (LinearLayout) findViewById(R.id.layout_register);
        layout_register.setOnClickListener(this);
        layout_login.setOnClickListener(this);
        button_login.setOnClickListener(this);
        button_register.setOnClickListener(this);

        //
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {
                View radioButton = radioGroup.findViewById(id);
                int index = radioGroup.indexOfChild(radioButton);
                if (index==0)gender = "Male";
                else gender = "Female";
            }
        });

        //Specialty Spinner//
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.profession_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        professionSpinner.setAdapter(adapter);
        //END professional Spinner//

        //Texts under CardView //
        String term_1 = getString(R.string.have_account1);
        String term_2 = getString(R.string.have_account2);
        String term_3 = getString(R.string.have_account3);

        SpannableString span = new SpannableString(term_1+" " + term_2+ " " + term_3);
        ClickableSpan span1click = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                StaticMethods.designRegisterLogin(txt_login, txt_register, view_1, view_2,
                        cardview_login, cardview_register, 2, LoginActivity.this);
            }
        };

        span.setSpan(span1click, span.length()- (term_2.length() + term_3.length() + 1),
                span.length() -term_3.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        txt_haveaccount.setText(span);
        txt_haveaccount.setMovementMethod(LinkMovementMethod.getInstance());

        String term_reg1 = getString(R.string.register_account1);
        String term_reg2 = getString(R.string.register_account2);
        SpannableString spanReg  = new SpannableString(term_reg1 +" " + term_reg2);
        ClickableSpan spanClick2 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                StaticMethods.designRegisterLogin(txt_login, txt_register, view_1, view_2,
                        cardview_login, cardview_register, 1, LoginActivity.this);
            }
        };
        spanReg.setSpan(spanClick2, spanReg.length() - term_reg2.length(),
                spanReg.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        txt_login_.setText(spanReg);
        txt_login_.setMovementMethod(LinkMovementMethod.getInstance());
        //END Texts under CardView //

    }

    public void login(final String email, String password, final int where,
                      final String username,  final String year,
                      final String specialty, final String gender){

        if (where==1){//Authentication
            if (!StaticMethods.isValidEmail(layout_login,email) ||
                    !StaticMethods.isValidPassword(layout_login,password)) return;
            dialogStatic.show();
            button_login.setEnabled(false);
        }//Else Auto-Authentication

        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                button_login.setEnabled(true);
                if (task.isSuccessful()){

                    final FirebaseUser user = task.getResult().getUser();

                    if (where ==2){//Only when register
                        Map<String, Object> map = new HashMap<>();
                        //map.put(PathFirebase.KEY_ID_PROVIDER, String.valueOf(new Date().getTime()));
                        map.put(PathFirebase.KEY_EMAIL, email);
                        map.put(PathFirebase.KEY_FULL_NAME, username);
                        map.put(PathFirebase.KEY_YEAR_STARTED_WORKING, year);
                        map.put(PathFirebase.KEY_PROVIDER, user.getProviderData());
                        map.put(PathFirebase.KEY_SPECIALTY, specialty);
                        map.put(PathFirebase.KEY_DATE_REGISTER, ServerValue.TIMESTAMP);
                        map.put(PathFirebase.KEY_GENDER, gender);
                        map.put(PathFirebase.KEY_CONNECTION, PathFirebase.CHILD_ONLINE);
                        map.put(PathFirebase.KEY_STATUS, "AVAILABLE");//Automatically be av.
                        map.put(PathFirebase.KEY_SCHEDULE, 1);//day
                        map.put(PathFirebase.kEY_PICK_DAYS, "1234567");//Mondey to sunday availables for default

                        SharedPreferences prefs = getSharedPreferences("FCMTOKENID", MODE_PRIVATE);
                        String gcmId = prefs.getString("gcmId", null);
                        map.put(PathFirebase.KEY_GCMID, gcmId);

                        pathFirebase.child(PathFirebase.PATH_PROVIDER).child(user.getUid()).setValue(map);

                        new ProviderManager(LoginActivity.this).saveDataProvider(username,
                                email, specialty, gender, user.getUid(), 1, "1234567"); // 1 = day

                        dialogStatic.dissmis();
                        Intent intent = new Intent(LoginActivity.this, ProviderDrawer.class);
                        startActivity(intent);
                        finish();
                    }else {//Login

                        pathFirebase.child(PathFirebase.PATH_PROVIDER).child(user.getUid())
                            .addListenerForSingleValueEvent(

                                new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if (dialogStatic!=null) dialogStatic.dissmis();

                                        if (dataSnapshot.exists()){

                                            Provider provider = dataSnapshot.getValue(Provider.class);

                                            new ProviderManager(LoginActivity.this)
                                                .saveDataProvider(provider.getFullName(), email, provider.getSpecialty(),
                                                provider.getGender(), user.getUid(), provider.getSchedule_attention(), provider.getPick_days());

                                            Intent intent = new Intent(LoginActivity.this, ProviderDrawer.class);
                                            intent.putExtra("user", provider);
                                            startActivity(intent);


                                            /** en Login actualizamos el gcmId***/
                                            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                                            Map<String, Object> updates = new HashMap<>();
                                            updates.put("gcmId",refreshedToken );
                                            //updates.put("online", true);
                                            if (refreshedToken!=null && !refreshedToken.isEmpty()){
                                                pathFirebase.child("providers").child(user.getUid()).updateChildren(updates);
                                            }

                                            finish();
                                        }else {//when login with account user => stop
                                            signOut();
                                        }

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        if (dialogStatic!=null) dialogStatic.dissmis();
                                        Toast.makeText(LoginActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                                        signOut();
                                    }
                                }
                            );
                    }

                } else {
                    dialogStatic.dissmis();
                    StaticMethods.showErrorMessageToUser( getString(R.string.error_auth),
                            LoginActivity.this);
                }
            }
        });
    }

    public void register(){

        final String email = email_register.getText().toString().trim();
        final String password = password_register.getText().toString().trim();
        final String username = edit_fullname.getText().toString().trim();
        final String year = year_counseling.getText().toString().trim();
        //if (professionSpinner.getSelectedItem()!=null)
        final String profession = professionSpinner.getSelectedItem().toString();

        System.out.println("Salida specialty:::: " + profession);

        if (!StaticMethods.isValidEmail(layout_login,email) ||
                !StaticMethods.isValidPassword(layout_login,password) ||
                !StaticMethods.isValidFullName(username, layout_register) ||
                !StaticMethods.isValidYear(year, layout_register) ||
                !StaticMethods.isValidSpinner(profession, layout_register)) return;

        dialogStatic.show();
        button_register.setEnabled(false);

        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    login(email, password, 2, username, year, profession, gender);
                } else {
                    dialogStatic.dissmis();
                    button_login.setEnabled(true);
                    StaticMethods.showErrorMessageToUser( task.getException().getMessage(), LoginActivity.this);
                    //StaticMethods.showErrorMessageToUser( getString(R.string.error_register), LoginActivity.this);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.layout_login:
                StaticMethods.designRegisterLogin(txt_login, txt_register, view_1,
                        view_2, cardview_login, cardview_register, 1, LoginActivity.this);
                break;
            case R.id.layout_register:
                StaticMethods.designRegisterLogin(txt_login, txt_register, view_1,
                        view_2, cardview_login, cardview_register, 2, LoginActivity.this);
                break;


            case R.id.button_login:
                String email = email_login.getText().toString();
                String password = password_login.getText().toString();
                login(email, password, 1, "","","", "");
                break;

            case R.id.button_register:
                register();
                break;
        }
    }


    private void loadDataServiceType(){
        pathFirebase.child(PathFirebase.PATH_USER)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        System.out.println("Salida specialty list: " );
                        if (dataSnapshot.exists()){
                            Specialty specialty =  dataSnapshot.getValue(Specialty.class);
                            System.out.println("Salida specialty list == " + specialty.getCareer() );
                            listSpecialty.add(specialty);
                            adapterSpinn.notifyDataSetChanged();
                        }else
                            System.out.println("Salida specialty list vacia: " );
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        System.out.println("Salida specialty error: " );
                    }
                });


        pathFirebase.child(PathFirebase.PATH_USER).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Specialty> list = new ArrayList<>();
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    Specialty specialty = postSnapshot.getValue(Specialty.class);
                    System.out.println("Salida specialty list == " + specialty.getCareer() );
                    list.add(specialty);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("Salida specialty list  errorrrr"  );
            }
        });


        pathFirebase.child(PathFirebase.PATH_USER).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                System.out.println("Salida : " + dataSnapshot.getValue() + " / " + s);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("Salida =====:  ");
            }
        });

        mainPathFB = FirebaseDatabase.getInstance().getReferenceFromUrl(PathFirebase.PATH_FIREBASE);
        recordPath = mainPathFB.child(PathFirebase.PATH_SPECIALTY);

        recordListener = recordPath.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.exists()){
                    System.out.println("Salida historial nulo -VV--------- " + s);
                }else {
                    System.out.println("Salida historial nulo  vvvvvvvv---------- ");
                }
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {


            }
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {}
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("Salida historial  XXXXXXXXX nulo ---------- ");
            }
        });


        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                System.out.println("Salida YEA Vacio::" );
                System.out.println("Salida YEA 2 Vacio::" + snapshot);
                //GenericTypeIndicator<ArrayList<Specialty>> t = new GenericTypeIndicator<ArrayList<Specialty>>() {};
                //ArrayList<Specialty> yourStringArray = snapshot.getValue(t);
                //Toast.makeText(LoginActivity.this,yourStringArray.get(0).getCareer(),Toast.LENGTH_LONG).show();
            }
            @Override
            public void onCancelled(DatabaseError firebaseError) {
                System.out.println("Salida NULOOOOOOOOOO Vacio::" );
            }
        };
        pathFirebase.child("users").addValueEventListener(postListener);

    }

    public void signOut() {
        auth.addAuthStateListener(authListener);
        auth.signOut();
    }

}
