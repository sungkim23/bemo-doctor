package com.tech.sungkim.bemoprovider.bemo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.tech.sungkim.bemoprovider.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //2s wait time until Login Activity begins
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                nextActivity();
                finish();
            }
        },1000);
    }

    //Intent for opening Login Activity
    public void nextActivity(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}