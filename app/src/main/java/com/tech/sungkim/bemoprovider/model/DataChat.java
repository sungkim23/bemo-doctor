package com.tech.sungkim.bemoprovider.model;

/**
 * Created by jacob on 1/11/2016.
 */

public class DataChat {

    //Con este objeto verificaremos si el provider esta en atencion
    private String idChat;
    private String status;//
    private String state; //available o unavailable
    private String idUser;

    public String getIdChat() {
        return idChat;
    }

    public void setIdChat(String idChat) {
        this.idChat = idChat;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }
}
