package com.tech.sungkim.bemoprovider.model;

/**
 * Created by jacob on 20/11/2016.
 */

public class Specialty {
    private String id;
    private String career;
    private String link;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCareer() {
        return career;
    }

    public void setCareer(String career) {
        this.career = career;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
