package com.tech.sungkim.bemoprovider.bemo;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tech.sungkim.bemoprovider.R;
import com.tech.sungkim.bemoprovider.util.PathFirebase;
import com.tech.sungkim.bemoprovider.util.ProviderManager;
import com.tech.sungkim.bemoprovider.util.StaticMethods;


public class AvailableActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    ActionBar actionBar;
    CardView card_day;
    CardView card_night;
    Button button_done;

    TextView txt_day, txt_night;
    ImageView img_day, img_night;
    RadioButton rbtn_night, rbtn_day;
    TextView txt_Select;
    View view_line1;
    View view_line2;

    int schedule ;//Day or night
    ProviderManager providerManager;
    DatabaseReference mainPath;
    String daysAvailables;//lunes - domingo


    CheckBox rbtn_monday;
    CheckBox rbtn_tuesday;
    CheckBox rbtn_wednesday;
    CheckBox rbtn_thursday;
    CheckBox rbtn_friday;
    CheckBox rbtn_saturday;
    CheckBox rbtn_sunday;
    String lunes = "",  martes = "", miercoles = "", jueves = "", viernes = "", sabado = "", domingo = "";
    String idProvider;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.available_job);
        ini();

        mainPath = FirebaseDatabase.getInstance().getReferenceFromUrl(PathFirebase.PATH_FIREBASE);
        providerManager = new ProviderManager(AvailableActivity.this);
        idProvider = providerManager.getSomeData(ProviderManager.KEY_PROVIDER);

        providerManager = new ProviderManager(AvailableActivity.this);
        if (providerManager.getIntData(PathFirebase.KEY_SCHEDULE)!=0)
            schedule = providerManager.getIntData(PathFirebase.KEY_SCHEDULE);

        daysAvailables = providerManager.getSomeData(PathFirebase.kEY_PICK_DAYS);

        if (daysAvailables!=null && daysAvailables.trim().length()>0){
            int daysCount = daysAvailables.trim().length();//days of week availables
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, daysCount);
            view_line1.setLayoutParams(param);
        }else {
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 0);
            view_line1.setLayoutParams(param);
        }


        if (schedule ==1){
            StaticMethods.selectChangeColor(card_day, txt_day, img_day, AvailableActivity.this, card_night, txt_night, img_night, 1);
            rbtn_day.setChecked(true);
            rbtn_night.setChecked(false);
        }else {
            StaticMethods.selectChangeColor(card_day, txt_day, img_day, AvailableActivity.this, card_night, txt_night, img_night, 0);
            rbtn_day.setChecked(false);
            rbtn_night.setChecked(true);
        }

        card_day.setOnClickListener(this);
        card_night.setOnClickListener(this);

    }


    private void ini(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(ResourcesCompat.getColor(getResources(), R.color.black, null));
        toolbar.setTitle("Bemo");
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.back_gray);

        card_day = (CardView) findViewById(R.id.card_day);
        card_night = (CardView) findViewById(R.id.card_night);
        button_done = (Button) findViewById(R.id.button_done);

        rbtn_day = (RadioButton) findViewById(R.id.rbtn_day);
        rbtn_night = (RadioButton) findViewById(R.id.rbtn_night);

        txt_day = (TextView) findViewById(R.id.txt_day);
        txt_night = (TextView) findViewById(R.id.txt_night);
        img_day = (ImageView) findViewById(R.id.img_day);
        img_night = (ImageView) findViewById(R.id.img_night);
        view_line2 = findViewById(R.id.view_line2);
        view_line1 = findViewById(R.id.view_line1);

        txt_Select = (TextView) findViewById(R.id.txt_Select);
        txt_Select.setOnClickListener(this);
        button_done.setOnClickListener(this);


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case android.R.id.home:
                AvailableActivity.this.finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.button_done:
                if (!StaticMethods.isInternetAvailable(AvailableActivity.this)) {
                    Toast.makeText(getApplicationContext(),"Don't have access to internet", Toast.LENGTH_LONG).show();
                    return;
                }
                mainPath.child(PathFirebase.PATH_PROVIDER).child(idProvider).child("schedule_attention").setValue(schedule);
                Toast.makeText(getApplicationContext(), "save successfully", Toast.LENGTH_LONG).show();
                providerManager.saveIntData(PathFirebase.KEY_SCHEDULE, schedule);
                AvailableActivity.this.finish();
                break;

            case R.id.card_day:
                schedule = 1;
                StaticMethods.selectChangeColor(card_day, txt_day, img_day, AvailableActivity.this, card_night, txt_night, img_night, 1);
                rbtn_day.setChecked(true);
                rbtn_night.setChecked(false);
                break;

            case R.id.card_night:
                schedule = 2;
                StaticMethods.selectChangeColor(card_day, txt_day, img_day, AvailableActivity.this, card_night, txt_night, img_night, 0);
                rbtn_day.setChecked(false);
                rbtn_night.setChecked(true);
                break;

            case R.id.txt_Select:
                showPopupDays();
                break;

            case R.id.rbtn_monday:
                if (rbtn_monday.isChecked())
                    lunes = "1";
                else lunes = "";
                break;

            case R.id.rbtn_tuesday:
                if (rbtn_tuesday.isChecked())
                    martes = "2";
                else martes = "";
                break;

            case R.id.rbtn_wednesday:
                if (rbtn_wednesday.isChecked())
                    miercoles = "3";
                else miercoles = "";
                break;
            case R.id.rbtn_thursday:
                if (rbtn_thursday.isChecked())
                    jueves = "4";
                else jueves = "";
                break;
            case R.id.rbtn_friday:
                if (rbtn_friday.isChecked())
                    viernes = "5";
                else viernes = "";
                break;
            case R.id.rbtn_saturday:
                if (rbtn_saturday.isChecked())
                    sabado = "6";
                else sabado = "";
                break;

            case R.id.rbtn_sunday:
                if (rbtn_sunday.isChecked())
                    domingo = "7";
                else domingo = "";
                break;
        }

    }




    private void showPopupDays(){
        final Dialog dialog =  new Dialog(AvailableActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_days);
        lunes = ""; martes = ""; miercoles =""; jueves = ""; viernes = ""; sabado = ""; domingo = "";
        daysAvailables = providerManager.getSomeData(PathFirebase.kEY_PICK_DAYS);
        System.out.println("Salida dialog day: " + daysAvailables);
        Button btnDone = (Button) dialog.findViewById(R.id.btn_agree);
        rbtn_monday = (CheckBox) dialog.findViewById(R.id.rbtn_monday);
        rbtn_tuesday = (CheckBox) dialog.findViewById(R.id.rbtn_tuesday);
        rbtn_wednesday = (CheckBox) dialog.findViewById(R.id.rbtn_wednesday);
        rbtn_thursday = (CheckBox) dialog.findViewById(R.id.rbtn_thursday);
        rbtn_friday = (CheckBox) dialog.findViewById(R.id.rbtn_friday);
        rbtn_saturday = (CheckBox) dialog.findViewById(R.id.rbtn_saturday);
        rbtn_sunday = (CheckBox) dialog.findViewById(R.id.rbtn_sunday);

        rbtn_monday.setOnClickListener(this);
        rbtn_tuesday.setOnClickListener(this);
        rbtn_wednesday.setOnClickListener(this);
        rbtn_thursday.setOnClickListener(this);
        rbtn_friday.setOnClickListener(this);
        rbtn_saturday.setOnClickListener(this);
        rbtn_sunday.setOnClickListener(this);
        if (daysAvailables!=null && daysAvailables.contains("1")) {
            rbtn_monday.setChecked(true); lunes = "1";
        }
        if (daysAvailables!=null && daysAvailables.contains("2")) {
            rbtn_tuesday.setChecked(true); martes = "2";
        }
        if (daysAvailables!=null && daysAvailables.contains("3")){
            rbtn_wednesday.setChecked(true); miercoles = "3";
        }
        if (daysAvailables!=null && daysAvailables.contains("4")) {
            rbtn_thursday.setChecked(true); jueves = "4";
        }
        if (daysAvailables!=null && daysAvailables.contains("5")) {
            rbtn_friday.setChecked(true); viernes = "5";
        }
        if (daysAvailables!=null && daysAvailables.contains("6")) {
            rbtn_saturday.setChecked(true); sabado = "6";
        }
        if (daysAvailables!=null && daysAvailables.contains("7")) {
            rbtn_sunday.setChecked(true); domingo = "7";
        }

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!StaticMethods.isInternetAvailable(AvailableActivity.this)) {
                    Toast.makeText(getApplicationContext(),"Don't have access to internet", Toast.LENGTH_LONG).show();
                    return;
                }

                daysAvailables = lunes + martes + miercoles + jueves + viernes + sabado + domingo;

                mainPath.child(PathFirebase.PATH_PROVIDER).child(idProvider).child("pick_days").setValue(daysAvailables);
                providerManager.saveSomeData(PathFirebase.kEY_PICK_DAYS, daysAvailables);
                Toast.makeText(AvailableActivity.this, "PICK DAYS SUCCESSFULLY", Toast.LENGTH_SHORT).show();
                dialog.dismiss();

                if (daysAvailables!=null && daysAvailables.trim().length()>0){
                    int daysCount = daysAvailables.trim().length();
                    LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, daysCount);
                    view_line1.setLayoutParams(param);
                }else {
                    LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 0);
                    view_line1.setLayoutParams(param);
                }
            }
        });
        dialog.show();

    }
}
