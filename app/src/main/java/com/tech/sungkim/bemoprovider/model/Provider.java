package com.tech.sungkim.bemoprovider.model;

import java.io.Serializable;

/** Created by jacob on 18/09/2016.
 */

public class Provider implements Serializable{

    private String id; //Id del provedor
    private String email;
    private String fullName;
    private String year;
    //provider(JsonObject? )
    private String specialty;
    private Long dateRegister;
    private String gender;
    private String connection;
    private String status;//Available or unavailable
    private String gcmId;
    private String image;
    private int schedule_attention;
    private String pick_days;

    private ChatAttention chatAttention;

    public ChatAttention getChatAttention() {
        return chatAttention;
    }

    public void setChatAttention(ChatAttention chatAttention) {
        this.chatAttention = chatAttention;
    }

//private String idProvider;


    public String getPick_days() {
        return pick_days;
    }

    public void setPick_days(String pick_days) {
        this.pick_days = pick_days;
    }

    public int getSchedule_attention() {
        return schedule_attention;
    }

    public void setSchedule_attention(int schedule_attention) {
        this.schedule_attention = schedule_attention;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public Long getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(Long dateRegister) {
        this.dateRegister = dateRegister;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getConnection() {
        return connection;
    }

    public void setConnection(String connection) {
        this.connection = connection;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGcmId() {
        return gcmId;
    }

    public void setGcmId(String gcmId) {
        this.gcmId = gcmId;
    }


}
