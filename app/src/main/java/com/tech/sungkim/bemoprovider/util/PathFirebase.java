package com.tech.sungkim.bemoprovider.util;


import com.google.firebase.auth.FirebaseAuth;

public class PathFirebase {


    public static final String PATH_FIREBASE = "https://bemo-45fc7.firebaseio.com/";
    public static final String PATH_PROVIDER = "providers";
    public static final String PATH_USER = "users";
    public static final String PATH_CHAT = "chats";
    public static final String PATH_SPECIALTY = "specialty";
    public static final String PATH_RECORD = "record";
    public static final String PATH_RECORD_USER ="recordUser";
    public static final String PATH_RECORD_PROVIDER = "recordProvider";

    public static final String KEY_PROVIDERecord = "providers";
   // public static final String KEY_USERecord = "users";

    public static final String KEY_GCMID =  "gcmId";

    public static final String KEY_ID_PROVIDER = "idProvider";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_FULL_NAME = "fullName";
    public static final String KEY_YEAR_STARTED_WORKING = "year";//Anio de experiencia
    public static final String KEY_PROVIDER = "provider";
    public static final String KEY_DATE_REGISTER = "dateRegister";
    public static final String KEY_GENDER =  "gender";
    public static final String KEY_SPECIALTY = "specialty";
    public static final String KEY_CONNECTION = "connection";
    public static final String KEY_STATUS = "status"; //available and  unavailable
    public static final String KEY_UNIQUE_ID = FirebaseAuth.getInstance().getCurrentUser().getUid();
    //Chat constants
    public static final String PATH_CHATS ="chat";
    public static final String KEY_SEND_USER="key_send_user";
    public static final String CHILD_ONLINE ="online";
    public static final String CHILD_OFFLINE ="offline";
    public static final String KEY_ONLINE="true";
    public static final String KEY_OFFLINE="false";
    public static final String KEY_CLOSE="key_close";

    public static final String KEY_SCHEDULE = "schedule_attention";//0 = 24 hours, 1 = day, 2 = night
    public static final String kEY_PICK_DAYS = "pick_days";
}
