package com.tech.sungkim.bemoprovider.model;

/**
   Created by jacob on 18/09/2016.
 */

public class MessageChat {
    private String message;
    private String receive;
    private String sender;
    private Boolean visto;
    private String name;
    private String timewrite;

    private int senderOrRecipient;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReceive() {
        return receive;
    }

    public void setReceive(String receive) {
        this.receive = receive;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public Boolean getVisto() {
        return visto;
    }

    public void setVisto(Boolean visto) {
        this.visto = visto;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTimewrite() {
        return timewrite;
    }

    public void setTimewrite(String timewrite) {
        this.timewrite = timewrite;
    }

    public int getSenderOrRecipient() {
        return senderOrRecipient;
    }

    public void setSenderOrRecipient(int senderOrRecipient) {
        this.senderOrRecipient = senderOrRecipient;
    }
}
