package com.tech.sungkim.bemoprovider.bemo;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.otto.Subscribe;
import com.tech.sungkim.bemoprovider.R;
import com.tech.sungkim.bemoprovider.adapters.AdapterRecord;
import com.tech.sungkim.bemoprovider.bus.BusConnection;
import com.tech.sungkim.bemoprovider.bus.BusProvider;
import com.tech.sungkim.bemoprovider.model.Provider;
import com.tech.sungkim.bemoprovider.model.RecordProvider;
import com.tech.sungkim.bemoprovider.model.User;
import com.tech.sungkim.bemoprovider.util.DialogStatic;
import com.tech.sungkim.bemoprovider.util.PathFirebase;
import com.tech.sungkim.bemoprovider.util.StaticMethods;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAuth auth;
    private FirebaseAuth.AuthStateListener authListener;

    private DatabaseReference connectionStatusPath;
    private DatabaseReference mainPathFB;
    private ValueEventListener connectedListener;
    private String idProvider;

    String idChat;
    String idRecord;
    public User user;
    Toolbar toolbar;
    public TextView txt_infoprovicer;
    public RecyclerView recyclerView;
    public AdapterRecord adapterRecord;
    public List<RecordProvider> listRecord;
    public List<String> listUpdate;
    public DatabaseReference recordPath;
    public DatabaseReference pathUserData;
    public ValueEventListener recordListener1;
    public ChildEventListener recordListener;

    public RelativeLayout layout_attention;
    public TextView txt_user_attention;
    DialogStatic dialogStatic;
    private User objUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ini();

        BusProvider.getInstance().register(this);
        mainPathFB = FirebaseDatabase.getInstance().getReferenceFromUrl(PathFirebase.PATH_FIREBASE);
        recordPath = mainPathFB.child(PathFirebase.PATH_RECORD).child(PathFirebase.PATH_RECORD_PROVIDER);


        String action = getIntent().getAction();
        if (action!=null) {//Ingresa automaticamente al chat( si viene de la notificacion)

            idChat =  getIntent().getExtras().getString("keyPathMessage");
            idRecord = getIntent().getExtras().getString("idRecord");
            user =  new User();
            user.setFullName( getIntent().getExtras().getString("username"));
            user.setIdUser(getIntent().getExtras().getString("iduser"));
            user.setEmail(getIntent().getExtras().getString("email"));
            user.setGcmId(getIntent().getExtras().getString("fcmIdUser"));

            if (user.getIdUser()!=null && !user.getIdUser().isEmpty()){
                Intent intent = new Intent(MainActivity.this, ChatView.class);
                intent.putExtra("session","NOTIFICATION");
                intent.putExtra("user", user);
                intent.putExtra("idChat", idChat);
                intent.putExtra("idRecord", idRecord);
                startActivity(intent);
            }
            layout_attention.setVisibility(View.VISIBLE);
            txt_user_attention.setText(user.getFullName());
        }else {
            layout_attention.setVisibility(View.GONE);
            txt_user_attention.setText("");
        }
        dialogStatic.show();
        System.out.println("Salida mainActivity Oncreate");
        //Necessary for LogOut to work
        auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser()!=null) idProvider = auth.getCurrentUser().getUid();

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    MainActivity.this.finish();
                }
            }
        };
        auth.addAuthStateListener(authListener);


        listUpdate = new ArrayList<>();
        listRecord = new ArrayList<>();
        adapterRecord = new AdapterRecord(MainActivity.this, listRecord);
        recyclerView.setAdapter(adapterRecord);

        //Por el momento solo mostraremos las atenciones que hizo el provider sin ver los detalles del chat
        //Query query = recordPath.orderByChild("idProvider").equalTo(idProvider);
        //query.addChildEventListener(new ChildEventListener()...   or
        System.out.println("Salida idProvider: " + idProvider );

        recordListener = recordPath.child(idProvider).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.exists()){
                    txt_infoprovicer.setVisibility(View.GONE);
                    RecordProvider record = dataSnapshot.getValue(RecordProvider.class);
                    record.setId(dataSnapshot.getKey());

                    System.out.println("Salida user: " + record.getNameUser() + " / " + record.getState());
                    if (record.getState()!=null && record.getState().equals("CLOSE")){
                        adapterRecord.setfill(record);
                        //listRecord.add(record);
                        //listUpdate.add(record);
                    }else {
                        listUpdate.add(record.getId());//Solo los q estan abiertos(ya q son los unicos q pueden cambiar de estado)
                    }

                    layout_attention.setVisibility(View.GONE);//Si ya se atendio => ocultamos la vista
                    txt_user_attention.setText(""); //Si vuelve a atenderse => se mostrara cuando haga click en la noif

                }else {
                    txt_infoprovicer.setVisibility(View.VISIBLE);
                    System.out.println("Salida historial nulo ---------- ");
                }
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                //Mejorar las actualizaciones para q no se repitan en el record
                /**if (dataSnapshot.exists()){
                    RecordProvider obj =  dataSnapshot.getValue(RecordProvider.class);
                    String id = dataSnapshot.getKey();
                    System.out.println("Salida change value record: " + obj.getNameUser());
                    obj.setId(id);
                    if (obj.getStatus().equals("CLOSE")){
                        adapterRecord.addFirstPosition(obj);
                    }
                }*/
            }
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {}
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });




        /** Verificamos si esta en atencion */
        mainPathFB.child(PathFirebase.PATH_PROVIDER).child(idProvider)
                .addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    Provider obj = dataSnapshot.getValue(Provider.class);

                    if (obj.getStatus()!=null && obj.getStatus().equals("UNAVAILABLE")){
                        //Hay 2 posibilidade(Q este en atencion o fuera de atencion)
                        System.out.println("Salida android prov: " + obj.getStatus());

                        if (obj.getChatAttention()!=null && !obj.getChatAttention().getState().equals("CLOSE")){ //OPEN

                            System.out.println("Salida android open: " + obj.getChatAttention().getState() + " / " + obj.getChatAttention().getIdUser());
                            //DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                            mainPathFB.child(PathFirebase.PATH_USER).child(obj.getChatAttention().getIdUser())
                                    .addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            System.out.println("Salida user user online");
                                            if (dataSnapshot.exists()){
                                                objUser = dataSnapshot.getValue(User.class);
                                                objUser.setIdUser(dataSnapshot.getKey()); //set IdUser = key or path
                                                layout_attention.setVisibility(View.VISIBLE);
                                                txt_user_attention.setText(objUser.getFullName());
                                            }
                                            dialogStatic.dissmis();
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                            dialogStatic.dissmis();
                                        }
                                    });
                        }else {//CLOSE
                            //System.out.println("Salida android close 2: " + obj.getChatAttention().getState());
                            layout_attention.setVisibility(View.GONE);
                            txt_user_attention.setText("");
                            dialogStatic.dissmis();
                            txt_infoprovicer.setText("Close and open app");
                            txt_infoprovicer.setVisibility(View.VISIBLE);
                        }

                    }else{//***Falta analizar la logica*//*//Available(cuando esta disponible)
                        layout_attention.setVisibility(View.GONE);
                        txt_user_attention.setText("");
                        dialogStatic.dissmis();
                    }

                }else dialogStatic.dissmis();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("Salida onCancelled provider:");
                dialogStatic.dissmis();
            }
        });



        connectionToFirebase();
    }

    public void showDialog(){
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_logout);
        dialog.show();
        Button buttonLogOut = (Button)dialog.findViewById(R.id.buttonLogOut);
        Button buttonCancel = (Button)dialog.findViewById(R.id.buttonCancel);
        buttonLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                signOut();
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void signOut() {
        //signOutRef.child(PathFirebase.KEY_CONNECTION).setValue(PathFirebase.KEY_OFFLINE);
        auth.signOut();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void ini(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        //getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_gray);
        getSupportActionBar().setTitle("Bemo Provider");
        toolbar.setTitleTextColor(ContextCompat.getColor(getApplicationContext(), R.color.gray_dark));

        txt_user_attention = (TextView) findViewById(R.id.txt_user_attention);
        layout_attention = (RelativeLayout) findViewById(R.id.layout_attention);

        txt_infoprovicer = (TextView) findViewById(R.id.txt_infoprovicer);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

         layout_attention.setOnClickListener(this);
        dialogStatic = new DialogStatic(MainActivity.this, "", "Loading");
    }

    @Override
    protected void onDestroy() {
        System.out.println("Salida destroy");
        BusProvider.getInstance().unregister(this);
        if (authListener != null) auth.removeAuthStateListener(authListener);

        if(connectedListener!=null) {
            mainPathFB.getRoot().child(".info/connected").removeEventListener(connectedListener);
        }

        if (recordListener!=null && recordPath!=null)
            recordPath.removeEventListener(recordListener);
        super.onDestroy();
    }

    @Subscribe
    public void connectionUser(BusConnection conection){
        if (conection.connection){
            System.out.println("Salida desconectado");
        }else System.out.println("Salida conectado");
    }

    private void connectionToFirebase() {//update estado de offline to online

        connectionStatusPath = mainPathFB.child("providers").child(idProvider).child(PathFirebase.KEY_CONNECTION);
        connectedListener = mainPathFB.getRoot().child(".info/connected").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean connected = (Boolean) dataSnapshot.getValue();
                if (connected) {
                    System.out.println("Salida status 1: " + connected);
                    connectionStatusPath.setValue(PathFirebase.CHILD_ONLINE);
                    connectionStatusPath.onDisconnect().setValue(PathFirebase.CHILD_OFFLINE);
                }else  System.out.println("Salida status 2");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("Salida Cancel 2");
            }
        });

    }



    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.layout_attention:
                if (objUser!=null && objUser.getIdUser()!=null){
                    Intent intent = new Intent(MainActivity.this, ChatView.class);
                    intent.putExtra("session","PAST_SESSIONS");
                    intent.putExtra("user", objUser);
                    startActivity(intent);
                }else StaticMethods.msgSnackbar(recyclerView, "Please close app and then open.");

                break;

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_perfil, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.close_session:
                showDialog();
                break;

            case R.id.profile:
                Intent intent = new Intent(MainActivity.this, PerfilProvider.class);
                intent.putExtra("id", idProvider);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
