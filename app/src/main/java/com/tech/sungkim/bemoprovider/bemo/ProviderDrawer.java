package com.tech.sungkim.bemoprovider.bemo;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.otto.Subscribe;
import com.tech.sungkim.bemoprovider.R;
import com.tech.sungkim.bemoprovider.bus.BusConnection;
import com.tech.sungkim.bemoprovider.bus.BusFinishSession;
import com.tech.sungkim.bemoprovider.bus.BusProvider;
import com.tech.sungkim.bemoprovider.fragment.TabsProvider;
import com.tech.sungkim.bemoprovider.model.Provider;
import com.tech.sungkim.bemoprovider.model.User;
import com.tech.sungkim.bemoprovider.util.DialogStatic;
import com.tech.sungkim.bemoprovider.util.PathFirebase;
import com.tech.sungkim.bemoprovider.util.ProviderManager;
import com.tech.sungkim.bemoprovider.util.StaticMethods;

public class ProviderDrawer extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    DrawerLayout drawer;
    NavigationView navigationView;
    AvailableActivity mainFragment;
    TabsProvider tabsProvider;
    ActionBarDrawerToggle toggle;
    Toolbar toolbar;
    ActionBar actionBar;

    LinearLayout layoutInSession;
    ImageView imgProviderPhoto;
    TextView txtNameProvider;
    TextView txt_session;


    private FirebaseAuth auth;
    private FirebaseAuth.AuthStateListener authListener;

    private DatabaseReference connectionStatusPath;
    private DatabaseReference mainPathFB;
    private ValueEventListener connectedListener;
    private String idProvider;

    String idChat;
    String idRecord;
    public User user;
    public TextView txt_infoprovicer;
    public DatabaseReference recordPath;
    public ChildEventListener recordListener;

    DialogStatic dialogStatic;
    private User objUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        BusProvider.getInstance().register(this);
        ini();

        mainFragment = new AvailableActivity();
        tabsProvider =  new TabsProvider();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        drawer.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);toolbar.setTitle("Bemo provider");
            }
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);toolbar.setTitle("Bemo");
            }
        });

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState==null){
            navigationView.getMenu().findItem(R.id.nav_main).setChecked(false);
            setupDrawerContent(R.id.nav_main);
        }

        actionBar.setHomeAsUpIndicator(R.drawable.lines3);


        mainPathFB = FirebaseDatabase.getInstance().getReferenceFromUrl(PathFirebase.PATH_FIREBASE);
        recordPath = mainPathFB.child(PathFirebase.PATH_RECORD).child(PathFirebase.PATH_RECORD_PROVIDER);


        String action = getIntent().getAction();
        if (action!=null) {//Ingresa automaticamente al chat( si viene de la notificacion)

            idChat =  getIntent().getExtras().getString("keyPathMessage");
            idRecord = getIntent().getExtras().getString("idRecord");
            user =  new User();
            user.setFullName( getIntent().getExtras().getString("username"));
            user.setIdUser(getIntent().getExtras().getString("iduser"));
            user.setEmail(getIntent().getExtras().getString("email"));
            user.setGcmId(getIntent().getExtras().getString("fcmIdUser"));

            if (user.getIdUser()!=null && !user.getIdUser().isEmpty()){
                Intent intent = new Intent(ProviderDrawer.this, ChatView.class);
                intent.putExtra("session","NOTIFICATION");
                intent.putExtra("user", user);
                intent.putExtra("idChat", idChat);
                intent.putExtra("idRecord", idRecord);
                startActivity(intent);
            }
            layoutInSession.setVisibility(View.VISIBLE);
            txtNameProvider.setText(user.getFullName());
            txt_session.setText("Currently you are in session");
        }else {
            layoutInSession.setVisibility(View.GONE);
            txtNameProvider.setText("");
        }
        dialogStatic.show();
        System.out.println("Salida mainActivity Oncreate");
        //Necessary for LogOut to work
        auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser()!=null) idProvider = auth.getCurrentUser().getUid();

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    startActivity(new Intent(ProviderDrawer.this, LoginActivity.class));
                    ProviderDrawer.this.finish();
                }
            }
        };
        auth.addAuthStateListener(authListener);


        /** Verificamos si esta en atencion */
        mainPathFB.child(PathFirebase.PATH_PROVIDER).child(idProvider)
                .addValueEventListener(new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()){
                            Provider obj = dataSnapshot.getValue(Provider.class);

                            if (obj.getStatus()!=null && obj.getStatus().equals("UNAVAILABLE")){
                                //Hay 2 posibilidade(Q este en atencion o fuera de atencion)
                                System.out.println("Salida android prov: " + obj.getStatus());

                                if (obj.getChatAttention()!=null && !obj.getChatAttention().getState().equals("CLOSE")){ //OPEN

                                    System.out.println("Salida android open: " + obj.getChatAttention().getState() + " / " + obj.getChatAttention().getIdUser());
                                    //DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                                    mainPathFB.child(PathFirebase.PATH_USER).child(obj.getChatAttention().getIdUser())
                                            .addValueEventListener(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    System.out.println("Salida user user online");
                                                    if (dataSnapshot.exists()){
                                                        objUser = dataSnapshot.getValue(User.class);
                                                        objUser.setIdUser(dataSnapshot.getKey()); //set IdUser = key or path
                                                        layoutInSession.setVisibility(View.VISIBLE);
                                                        txtNameProvider.setText(objUser.getFullName());
                                                        txt_session.setText("Currently you are in session");
                                                    }
                                                    dialogStatic.dissmis();
                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {
                                                    dialogStatic.dissmis();
                                                }
                                            });
                                }else {//CLOSE
                                    //System.out.println("Salida android close 2: " + obj.getChatAttention().getState());
                                    layoutInSession.setVisibility(View.GONE);
                                    txtNameProvider.setText("");
                                    dialogStatic.dissmis();
                                    txt_infoprovicer.setText("Close and open app");
                                    txt_infoprovicer.setVisibility(View.VISIBLE);
                                }

                            }else{//***Falta analizar la logica*//*//Available(cuando esta disponible)
                                layoutInSession.setVisibility(View.GONE);
                                txtNameProvider.setText("");
                                dialogStatic.dissmis();
                            }

                        }else dialogStatic.dissmis();

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        System.out.println("Salida onCancelled provider:");
                        dialogStatic.dissmis();
                    }
                });

        connectionToFirebase();

    }


    private void ini(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(ResourcesCompat.getColor(getResources(), R.color.black, null));
        toolbar.setTitle("Bemo");
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        layoutInSession = (LinearLayout) findViewById(R.id.layout_session_attention);
        imgProviderPhoto = (ImageView) findViewById(R.id.img_providerPhoto);
        txtNameProvider = (TextView) findViewById(R.id.txt_nameProvider);
        txt_session = (TextView) findViewById(R.id.txt_session);

        layoutInSession.setOnClickListener(this);
        dialogStatic = new DialogStatic(ProviderDrawer.this, "", "Loading");
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.provider_drawer, menu);
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

       setupDrawerContent(item.getItemId());

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void setupDrawerContent(int position){

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (position){
            case R.id.nav_main:
                transaction.replace(R.id.container, tabsProvider);
                break;

            case R.id.nav_available:
                startActivity( new Intent(getApplicationContext(), AvailableActivity.class));
                break;

            case R.id.nav_profile:
                Intent intent = new Intent(ProviderDrawer.this, PerfilProvider.class);
                intent.putExtra("id", idProvider);
                startActivity(intent);

                break;

            case R.id.close_session:
                showDialog();
                break;
        }

        transaction.commit();
        drawer.closeDrawers();
    }


    public void showDialog(){
        final Dialog dialog = new Dialog(ProviderDrawer.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_logout);
        dialog.show();
        Button buttonLogOut = (Button)dialog.findViewById(R.id.buttonLogOut);
        Button buttonCancel = (Button)dialog.findViewById(R.id.buttonCancel);
        buttonLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                signOut();
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void signOut() {
        auth.signOut();
        new ProviderManager(ProviderDrawer.this).clearProviderData();
    }


    @Override
    protected void onDestroy() {
        System.out.println("Salida destroy");
        BusProvider.getInstance().unregister(this);
        if (authListener != null) auth.removeAuthStateListener(authListener);

        if(connectedListener!=null) {
            mainPathFB.getRoot().child(".info/connected").removeEventListener(connectedListener);
        }

        if (recordListener!=null && recordPath!=null)
            recordPath.removeEventListener(recordListener);
        super.onDestroy();
    }


    @Subscribe
    public void connectionUser(BusConnection conection){
        if (conection.connection){
            System.out.println("Salida desconectado");
        }else System.out.println("Salida conectado");
    }

    @Subscribe
    public void finishSessionUser(BusFinishSession value){
        if (value.finishSession){
            layoutInSession.setVisibility(View.GONE);
        }
    }

    private void connectionToFirebase() {//update estado de offline to online

        connectionStatusPath = mainPathFB.child("providers").child(idProvider).child(PathFirebase.KEY_CONNECTION);
        connectedListener = mainPathFB.getRoot().child(".info/connected").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean connected = (Boolean) dataSnapshot.getValue();
                if (connected) {
                    System.out.println("Salida status 1: " + connected);
                    connectionStatusPath.setValue(PathFirebase.CHILD_ONLINE);
                    connectionStatusPath.onDisconnect().setValue(PathFirebase.CHILD_OFFLINE);
                }else  System.out.println("Salida status 2");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("Salida Cancel 2");
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layout_session_attention:
                if (objUser!=null && objUser.getIdUser()!=null){
                    Intent intent = new Intent(ProviderDrawer.this, ChatView.class);
                    intent.putExtra("session","PAST_SESSIONS");
                    intent.putExtra("user", objUser);
                    startActivity(intent);
                }else StaticMethods.msgSnackbar(layoutInSession, "Please close app and then open.");

                break;

        }
    }
}
