package com.tech.sungkim.bemoprovider.bemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tech.sungkim.bemoprovider.R;
import com.tech.sungkim.bemoprovider.model.Provider;
import com.tech.sungkim.bemoprovider.util.PathFirebase;

public class PerfilProvider extends AppCompatActivity {

    String idProvider;
    TextView txt_name;
    TextView txt_email;
    TextView txt_gender;
    TextView txt_years;
    public Toolbar toolbar;

    DatabaseReference mRootRef;
    DatabaseReference pathProvider;
    ValueEventListener eventListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.perfil_provider);

        ini();

        Bundle bundle = getIntent().getExtras();
        if (bundle!=null) idProvider = bundle.getString("id");




        mRootRef = FirebaseDatabase.getInstance().getReference();
        pathProvider = mRootRef.child(PathFirebase.PATH_PROVIDER).child(idProvider);

        eventListener = pathProvider.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    Provider user =  dataSnapshot.getValue(Provider.class);
                    txt_name.setText(user.getFullName());
                    txt_email.setText(user.getEmail());
                    txt_years.setText(user.getYear());
                    txt_gender.setText(user.getGender());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void ini(){

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_gray);
        getSupportActionBar().setTitle("Profile");

        txt_name = (TextView) findViewById(R.id.txt_name);
        txt_years = (TextView) findViewById(R.id.txt_years);
        txt_email = (TextView) findViewById(R.id.txt_email);
        txt_gender = (TextView) findViewById(R.id.txt_gender);


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(eventListener!=null && pathProvider!=null) pathProvider.removeEventListener(eventListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_edit_profile:

                break;

            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

}
