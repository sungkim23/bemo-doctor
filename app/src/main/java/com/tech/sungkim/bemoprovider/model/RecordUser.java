package com.tech.sungkim.bemoprovider.model;

/** Created by jacob on 27/09/2016.
 */

public class RecordUser {

    /***Datos para mostrar las sesiones*/
    private String recordProvider;//Calificacion del provider
    private String recordUser;
    private String nroAttention;// numero de personas atendidas(calculo en la app)
    private long startDate;//Fecha de inicio del chat(momento en el q escribio el usuatio)
    private String timeConsult;//tiempo que se usa para hacer el calculo del tiempo transcurrido en el chat
    private long endDate;//Fecha final del chat
    private String state; //open, progressing, close y aborted(chat no atendido)

    private String nameUser;//full name
    private String idUser;
    private String idProvider;//es neces?
    private String comment;//Optional
    private String typeService;//tipo de servicio q se brindo al usuario
    private String imageUser; //    imagen del usuario
    private boolean saveProvider;//para verificar si el objeto ya fue creado x el prov//True = fue guardado, false = el obj no fue guardado en el historial
    private boolean saveUser; //para verificar si el objeto ya fue creado x el user

    private String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isSaveProvider() {
        return saveProvider;
    }

    public void setSaveProvider(boolean saveProvider) {
        this.saveProvider = saveProvider;
    }

    public boolean isSaveUser() {
        return saveUser;
    }

    public void setSaveUser(boolean saveUser) {
        this.saveUser = saveUser;
    }

    public String getImageUser() {
        return imageUser;
    }

    public void setImageUser(String imageUser) {
        this.imageUser = imageUser;
    }

    public String getRecordProvider() {
        return recordProvider;
    }

    public void setRecordProvider(String recordProvider) {
        this.recordProvider = recordProvider;
    }

    public String getRecordUser() {
        return recordUser;
    }

    public void setRecordUser(String recordUser) {
        this.recordUser = recordUser;
    }

    public String getNroAttention() {
        return nroAttention;
    }

    public void setNroAttention(String nroAttention) {
        this.nroAttention = nroAttention;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public String getTimeConsult() {
        return timeConsult;
    }

    public void setTimeConsult(String timeConsult) {
        this.timeConsult = timeConsult;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdProvider() {
        return idProvider;
    }

    public void setIdProvider(String idProvider) {
        this.idProvider = idProvider;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getTypeService() {
        return typeService;
    }

    public void setTypeService(String typeService) {
        this.typeService = typeService;
    }
}
